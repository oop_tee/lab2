import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input your score: ");
        int score = kb.nextInt();
        if (score >= 80) {
            System.out.println("Garde: A");
        } else if ((score >= 75) && (score < 80)) {
            System.out.println("Garde: B+");
        } else if ((score >= 70) && (score < 75)) {
            System.out.println("Garde: B");
        } else if ((score >= 65) && (score < 70)) {
            System.out.println("Garde: C+");
        } else if ((score >= 60) && (score < 65)) {
            System.out.println("Garde: C");
        } else if ((score >= 55) && (score < 60)) {
            System.out.println("Garde: D+");
        } else if ((score >= 50) && (score < 55)) {
            System.out.println("Garde: D");
        } else {
            System.out.println("Garde: F");
        }

    }

}
